#!/usr/bin/env python
import sys

mindepth = 10000
maxdepth = 0
firstyear = 3000
lastyear = 0
deepsamples = 0
for line in open(sys.argv[1],'r'):
	line = line.strip()
#	if not line: # skip blank lines
#		continue
	tabulardata = line.split("\t")
	try:
		depth = float(tabulardata[1])
		year = int(tabulardata[5].split("-")[0])
	except ValueError: # if depth and year cannot be found, skip
		continue
	if depth > maxdepth:
		maxdepth = depth
	if depth < mindepth:
		mindepth = depth
	if depth >= 1000:
		deepsamples += 1
	if year > lastyear:
		lastyear = year
	if year < firstyear:
		firstyear = year
print "shallowest sample at", mindepth
print "deepest sample at", maxdepth
print "first sample from", firstyear
print "last sample from", lastyear
print deepsamples, "samples from 1000m or more"