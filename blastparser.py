#!/usr/bin/env python

# sample script to retrieve fasta sequences that have a blast hit

import sys
import time
import argparse
from Bio import SeqIO

if len(sys.argv) < 2:
	sys.argv.append("-h")
parser = argparse.ArgumentParser()
parser.add_argument("-i","--input", help="FASTA format input file")
parser.add_argument("-b","--blast", help="tabular blast results file")
parser.add_argument("-e","--evalue", type=float, default=1.0, help="evalue cutoff for blast results")
parser.add_argument("-x","--exclude", action="store_true", help="instead write sequences without a blast hit")
args = parser.parse_args()

blastresults = {} # empty dictionary to store sequences to keep
print >> sys.stderr, "reading blast results from", args.blast
for line in open(args.blast, "r"):
	columns = line.split("\t") # columns is a list of strings
	evalue = float(columns[10])
	if evalue <= args.evalue: # if yes, then keep the ID
		seqid = columns[0]
		blastresults[seqid] = True
print >> sys.stderr, "kept IDs for {} matches".format( len(blastresults) )

resultcount = 0
totalcount = 0
print >> sys.stderr, "parsing fasta from", args.input
for seqrecord in SeqIO.parse(args.input,"fasta"):
	totalcount += 1
	if args.exclude: # meaning instead keep sequences without blast hits
		if seqrecord.id not in blastresults: # if key is not in dictionary
			resultcount += 1
			sys.stdout.write( seqrecord.format("fasta") )
	else: # meaning not using -x
		if seqrecord.id in blastresults: # if key is indeed in dictionary
			resultcount += 1
			sys.stdout.write( seqrecord.format("fasta") )
print >> sys.stderr, "counted {} total proteins".format( totalcount )
print >> sys.stderr, "wrote {} proteins".format( resultcount )

