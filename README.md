# Practical Computing for Biologists at PalMuc #

Based off of the [original PCfB](http://practicalcomputing.org/). There is a useful reference guide (as PDF) for Python, shell, and regular expressions that can be downloaded [here](http://practicalcomputing.org/files/PCfB_Appendices.pdf).

Downloads for the course and example files are found in the [Downloads](https://bitbucket.org/wrf/pcfb/downloads) tab.

Other information about [downloading and installing Python](https://wiki.python.org/moin/BeginnersGuide/Download), as well as an [introduction to Python](https://docs.python.org/2/tutorial/introduction.html) are available.

Check here for [instructions for getting BioPython](http://biopython.org/wiki/Documentation), as well as [sample code and examples](http://biopython.org/DIST/docs/tutorial/Tutorial.html).

### Starting with argparse ###

```
#!python

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-i","--input", help="FASTA format input file")
args = parser.parse_args()
```