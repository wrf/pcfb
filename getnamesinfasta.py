#!/usr/bin/env python

import sys
import time
import argparse
from Bio import SeqIO

if len(sys.argv) < 2:
	sys.argv.append("-h")
parser = argparse.ArgumentParser()
parser.add_argument("-i","--input", help="FASTA format input file")
parser.add_argument("-s","--species", nargs="*", help="5 letter species code from Uniprot, such as HUMAN")
# -s HUMAN MOUSE BOVIN
args = parser.parse_args()

resultcount = 0
print >> sys.stderr, "reading sequences from", args.input, time.asctime()
for seqrecord in SeqIO.parse(args.input, "fasta"):
	fullid = seqrecord.id # full id is sp|Q2TBW7|SNX2_BOVIN
	splitid = fullid.split("|") # this is a list, ["sp", "Q2TBW7", "SNX2_BOVIN"]
	databasename = splitid[0] # would be "sp"
	speciesname = splitid[2].split("_")[1] # item 0 is sp|Q2TBW7|SNX2, 1 is BOVIN
	if speciesname in args.species: # args.species is a list
		resultcount += 1
		# same as resultcount = resultcount + 1
		sys.stdout.write( seqrecord.format( "fasta" ) )
print >> sys.stderr, "found {} {} proteins".format( resultcount, args.species ), time.asctime()