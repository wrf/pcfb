#!/usr/bin/env python
import sys

seqID = "" # set up empty string to be overwritten by the first value
sequence = ""
for line in open(sys.argv[1],"r"):
	line = line.strip() # remove excess white space characters
	if not line: # skip blank lines
		continue
	if line[0] == ">": # if first character is >
		if seqID: # meaning not the first sequence
			seqlength = len(sequence) # get length of previous sequence
			print seqID, seqlength
		seqID = line[1:] # take all characters after the 1st
		sequence = "" # reset the sequence
	else: # meaning not a header line, so contains sequence
		sequence += line